<?php
namespace App\Traits;

use App\Models\Role;
use App\Models\Permission;

/**
 *
 */
trait HasRolesAndPermitions
{
  public function permissions(){
    return $this->belongsToMany(Permission::class,'users_permissions');
  }

  public function roles(){
    return $this->belongsToMany(Role::class,'users_roles');
  }
  public function hasRole(...$roles){
    foreach ($roles as $role) {
      if ($this->roles->contains('slug',$role)) {
        return true;
      }
    }
    return false;
  }

  public function hasPermission($permissions){
    return (bool) $this->permissions()->where('slug', $permissions)->count();
  }

  public function hasPermissionTo($permissions){
    return $this->hasPermissionThroughRole($permissions) || $this->hasPermission($permissions->slug);
}

  public function hasPermissionThroughRole($permissions)
  {
    foreach ($permissions->roles as $role){
        if($this->roles->contains($role)) {
            return true;
        }
    }
    return false;
  }

public function getAllPermissions(array $permissions)
{
    return Permition::wherein('slug',$permissions)->get();
}

public function givePermissionsTo($permissions)
{
  $permissions = getAllPermissions($permissions);
  if($permissions==null){
    return $this;
  }
   $this->permissions()->saveMany($permissions);
   return $this;
}

public function deletePermissions(... $permissions)
{
    $permissions = getAllPermissions($permissions);
    $this->permissions()->detach($permissions);
    return $this;
}
public function refreshPermissions(... $permissions)
{
  $this->permissions()->detach();
  return $this->givePermissionsTo($permissions);
}
}
