<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next,$permission=null)
    {
      if(auth()->user()==null||$permission==null){
        abort(404);
        return $next($request);
      }
      if(  !auth()->user()->can($permission)) {
          abort(404);
      }
        return $next($request);
    }
}
