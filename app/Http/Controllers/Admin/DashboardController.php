<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Article;
use App\Models\User;

class DashboardController extends Controller
{
    public function dashboard(){
        return view('admin.dashboard',[
          'categories'=>Category::lastCategories(5),
          'articles'=>Article::lastArticle(5),
          'countArticle'=>Article::count(),
          'countCategory'=>Category::count(),
          'countUser'=>User::count()
        ]);
    }
}
