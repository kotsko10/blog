@extends('Admin.layouts.app_admin')

@section('content')
<div class="container">
  <div class="row">
      <div class="col-sm-3">
        <div class="jumbotron">
          <p><span class="label labrl-primary">Категорії {{$countCategory}}<span></p>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="jumbotron">
          <p><span class="label labrl-primary">Матеріали {{$countArticle}}<span></p>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="jumbotron">
          <p><span class="label labrl-primary">Користувачів {{$countUser}}<span></p>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="jumbotron">
          <p><span class="label labrl-primary">Сьогодні 0<span></p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        @if(Gate::allows('category_create'))
        <a href="{{route('admin.category.create')}}" class="btn ctn-block btn-default"> Створити категорію</a>
      @endif
        @foreach ($categories as $category)
          <a class="list-group-item" href="{{route("admin.category.edit",$category)}}">
            <h4 class="list-group-item-heading">{{$category->title}}</h4>
            <p class="list-group-item-text">
            {{$category->articles()->count()}}
            </p>
          </a>
        @endforeach

      </div>
      <div class="col-sm-6">
        @if(Gate::allows('article_create'))
        <a href="{{route('admin.article.create')}}" class="btn ctn-block btn-default "> Створити новину</a>
        @endif
        @foreach ($articles as $article)
          <a class="list-group-item" href="{{route("admin.article.edit",$article)}}">
            <h4 class="list-group-item-heading">{{$article->title}}</h4>
            <p class="list-group-item-text">
            {{$article->categories()->pluck('title')->implode(", ")}}
            </p>
          </a>
        @endforeach
      </div>
    </div>
  </div>
</div>
@endsection
