@extends('admin.layouts.app_admin')

@section('content')

  <div class="container">
    @component('admin.components.breadcrumb')
      @slot('title') Створення статі  @endslot
      @slot('parent') Головна  @endslot
      @slot('active') Новини @endslot
    @endcomponent
    <hr>
    <form  class="form-horizontal" action="{{route('admin.article.store')}}" method="post">
      {{csrf_field()}}
      {{-- include form --}}
      @include('admin.articles.partials.form')
      <input type="hidden" name="create_by" value="{{Auth::id()}}">
    </form>
  </div>
@endsection
