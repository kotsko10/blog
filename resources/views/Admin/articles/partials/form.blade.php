<label for="">Статус</label>
<select class="form-control" name="published">
  @if (isset($article->id))
    <option value="0" @if ($article->published == 0) selected="" @endif>Не опубликовано</option>
    <option value="1" @if ($article->published == 1) selected="" @endif>Опубликовано</option>
  @else
    <option value="0">Не опубликовано</option>
    <option value="1">Опубликовано</option>
  @endif
</select>

<label for="">Заголовок</label>
<input type="text" class="form-control" name="title" placeholder="Заголовок категориї" value="{{$article->title ?? ""}}" required>

<label for="">Slug (унікальне значення)</label>
<input class="form-control" type="text" name="slug" placeholder="Автоматична генерація" value="{{$article->slug ?? ""}}" readonly="">

<label for="">Батьківська категорія</label>
<select class="form-control" name="categories[]" multiple="">
  @include('admin.articles.partials.categories', ['categories' => $categories])
</select>
<label for="">Короткий опис</label>
<textarea class="form-control z-depth-1"  name="description_short" id="exampleFormControlTextarea6" rows="3" placeholder="Короткий опис">{{$article->description_short ?? ""}}</textarea>

<label for="">Повний опис</label>
<textarea class="form-control z-depth-1"  name="description" id="exampleFormControlTextarea6" rows="3" placeholder="Повний опис">{{$article->description ?? ""}}</textarea>

<hr />
<label for="">Мета заголовок</label>
<input type="text" class="form-control" name="meta_title" placeholder="Мета заголовок" value="{{$article->meta_title ?? ""}}" >
<hr />
<label for="">Мета опис</label>
<input type="text" class="form-control" name="meta_description" placeholder="Мета опис" value="{{$article->meta_description ?? ""}}" >
<hr />
<label for="">Ключові слова</label>
<input type="text" class="form-control" name="meta_keyword" placeholder="Ключові слова" value="{{$article->meta_keyword ?? ""}}" >

<hr />

<input class="btn btn-primary" type="submit" value="Зберегти">
