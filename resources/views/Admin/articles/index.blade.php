@extends('Admin.layouts.app_admin')

@section('content')
  <div class="container">

    @component('Admin.components.breadcrumb')
      @slot('title') Список статей  @endslot
      @slot('parent') Головна  @endslot
      @slot('active') Матеріали  @endslot
    @endcomponent
    <hr>
    @if(Gate::allows('article_create'))
    <a href="{{route('admin.article.create')}}" class="btn btn-primary pull-right"><i class="fafa-plus-square-o"></i>Створити статтю</a>
    @endif
    <table class="table table-striped">
      <thead>
        <th>Назва</th>
        <th>Категорії</th>
        <th class="text-right">Дії</th>
      </thead>
      <tbody>
        @forelse ($articles as $article)
       <tr>
         <td><a href="{{route("article",$article->slug)}}">{{$article->title}}</a></td>
         <td>
          {{$article->categories()->pluck('title')->implode(", ")}}
      </td>
         <td>
           <form onsubmit="if(confirm('Видалити?')){return true}else{return false}" action="{{route('admin.article.destroy',$article)}}" method="post">
            <input type="hidden" name="_method" value="DELETE">
            {{csrf_field()}}
              @if(Gate::allows('article_edit'))
             <a href="{{route('admin.article.edit',$article)}}"><i class="fa fa-edit"></i></a>
             @endif
             @if(Gate::allows('article_delete'))
              <button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
              @endif
           </form>
          </td>
       </tr>
     @empty
       <tr>
         <td colspan="3" class="text-center"><h2>Дані відсутні</h2></td>
       </tr>
     @endforelse
      </tbody>
      <tfoot>
        <tr>
          <td colspan="3">
            <ul class="pagination  pull-right">
              {{ $articles->links('pagination::bootstrap-4')}}
            </ul>
          </td>
        </tr>
      </tfoot>
    </table>

  </div>
@endsection
