@extends('admin.layouts.app_admin')

@section('content')

  <div class="container">
    @component('admin.components.breadcrumb')
      @slot('title') Редагування статі  @endslot
      @slot('parent') Головна  @endslot
      @slot('active') Новини @endslot
    @endcomponent
    <hr>
    <form  class="form-horizontal" action="{{route('admin.article.update',$article)}}" method="post">
        <input  type="hidden" name="_method" value="put">
      {{csrf_field()}}
      {{-- include form --}}
      @include('admin.articles.partials.form')
      <input type="hidden" name="modified_by" value="{{Auth::id()}}">
    </form>
  </div>
@endsection
