@extends('admin.layouts.app_admin')

@section('content')

  <div class="container">
    @component('admin.components.breadcrumb')
      @slot('title') Створення категорії  @endslot
      @slot('parent') Головна  @endslot
      @slot('active') Категорії @endslot
    @endcomponent
    <hr>
    <form  class="form-horizontal" action="{{route('admin.category.store')}}" method="post">
      {{csrf_field()}}
      {{-- include form --}}
      @include('admin.categories.partials.form')
      <input type="hidden" name="created_by" value="{{Auth::id()}}">
    </form>
  </div>
@endsection
