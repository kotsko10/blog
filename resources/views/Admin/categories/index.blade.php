@extends('Admin.layouts.app_admin')

@section('content')
  <div class="container">

    @component('Admin.components.breadcrumb')
      @slot('title') Список категорій  @endslot
      @slot('parent') Головна  @endslot
      @slot('active') Категорії  @endslot
    @endcomponent
    <hr>
    @if(Gate::allows('category_create'))
    <a href="{{route('admin.category.create')}}" class="btn btn-primary pull-right"><i class="fafa-plus-square-o"></i>Створити категорію</a>
  @endif
    <table class="table table-striped">
      <thead>
        <th>Назва</th>
        <th>Публікації</th>
        <th class="text-right">Дії</th>
      </thead>
      <tbody>
        @forelse ($categories as $category)
       <tr>
         <td>{{$category->title}}</td>
         <td>{{$category->articles()->count()}}</td>
         <td>
           <form onsubmit="if(confirm('Видалити?')){return true}else{return false}" action="{{route('admin.category.destroy',$category)}}" method="post">
            <input type="hidden" name="_method" value="DELETE">
            {{csrf_field()}}
            @if(Gate::allows('category_edit'))
             <a href="{{route('admin.category.edit',$category)}}"><i class="fa fa-edit"></i></a>
           @endif
           @if(Gate::allows('category_delete'))
              <button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
            @endif
           </form>
          </td>
       </tr>
     @empty
       <tr>
         <td colspan="3" class="text-center"><h2>Дані відсутні</h2></td>
       </tr>
     @endforelse
      </tbody>
      <tfoot>
        <tr>
          <td colspan="3">
            <ul class="pagination pull-right">
              {{$categories->links('pagination::bootstrap-4')}}
            </ul>
          </td>
        </tr>
      </tfoot>
    </table>

  </div>
@endsection
