@extends('Admin.layouts.app_admin')

@section('content')
  <div class="container">

    @component('Admin.components.breadcrumb')
      @slot('title') Список Користувачів  @endslot
      @slot('parent') Головна  @endslot
      @slot('active') Користувачі  @endslot
    @endcomponent
    <hr>
    @if(Gate::allows('user_create'))
    <a href="{{route('admin.user_managment.user.create')}}" class="btn btn-primary pull-right"><i class="fafa-plus-square-o"></i>Створити користувача</a>
  @endif
    <table class="table table-striped">
      <thead>
        <th>Імя</th>
        <th>E-mail</th>
        <th class="text-right">Дії</th>
      </thead>
      <tbody>
        @forelse ($users as $user)
       <tr>
         <td>{{$user->name}}</td>
         <td>{{$user->email}}</td>
         <td>
           <form onsubmit="if(confirm('Видалити?')){return true}else{return false}" action="{{route('admin.user_managment.user.destroy',$user)}}" method="post">
            {{method_field('DELETE')}}
            {{csrf_field()}}
            @if(Gate::allows('user_edit'))
             <a href="{{route('admin.user_managment.user.edit',$user)}}"><i class="fa fa-edit"></i></a>
           @endif
           @if(Gate::allows('user_delete'))
              <button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
            @endif
           </form>
          </td>
       </tr>
     @empty
       <tr>
         <td colspan="3" class="text-center"><h2>Дані відсутні</h2></td>
       </tr>
     @endforelse
      </tbody>
      <tfoot>
        <tr>
          <td colspan="3">
            <ul class="pagination pull-right">
              {{$users->links('pagination::bootstrap-4')}}
            </ul>
          </td>
        </tr>
      </tfoot>
    </table>

  </div>
@endsection
