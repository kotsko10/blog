@extends('admin.layouts.app_admin')

@section('content')

  <div class="container">
    @component('admin.components.breadcrumb')
      @slot('title') Редагування користувача  @endslot
      @slot('parent') Головна  @endslot
      @slot('active') Користувач @endslot
    @endcomponent
    <hr>
    <form  class="form-horizontal" action="{{route('admin.user_managment.user.update',$user)}}" method="post">
      {{method_field('PUT')}}
      {{csrf_field()}}
      {{-- include form --}}
      @include('admin.user_managment.users.partials.form')
    </form>
  </div>
@endsection
