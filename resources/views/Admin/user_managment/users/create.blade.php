@extends('admin.layouts.app_admin')

@section('content')

  <div class="container">
    @component('admin.components.breadcrumb')
      @slot('title') Створення користувача  @endslot
      @slot('parent') Головна  @endslot
      @slot('active') Користувач @endslot
    @endcomponent
    <hr>
    <form  class="form-horizontal" action="{{route('admin.user_managment.user.store')}}" method="post">
      {{csrf_field()}}
      {{-- include form --}}
      @include('admin.user_managment.users.partials.form')
    </form>
  </div>
@endsection
