@extends('layouts.app')

@section('title'){{$category->title}} - MyBlog @endsection


@section('content')
  <div class="container">
    @forelse ($articles as $article)
      <div class="row">
          <div class="col-sm-12">
          <h2><a href="{{route("article",$article->slug)}}">{{$article->title}}</a></h2>
          <p>{!!$article->description_short !!}</p>
          </div>
      </div>
    @empty
      <h1 class="text-center">Пусто</h1>
    @endforelse
      {{ $articles->links('pagination::bootstrap-4')}}
  </div>


@endsection
