@foreach ($categories as $category)
  @if ($category->children->where("published",1)->count())
    <li class="nav-item dropdown">
        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="{{url("/blog/category/$category->slug")}}" role="button"  aria-haspopup="true" aria-expanded="false" v-pre>
            {{$category->title}}
        </a>
        <ul class="dropdown-menu"role="menu">
          @include('layouts.top_menu',["categories"=>$category->children])
        </ul>

    @else
      <li>
        <a  class="nav-link " href="{{url("/blog/category/$category->slug")}}" role="button"  aria-haspopup="true" aria-expanded="false" v-pre>
            {{$category->title}}
        </a>
  @endif
  </li>
@endforeach


{{--
      <a  href="#" >Блог</a>
      <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
        <li ><a class="dropdown-item" href="{{route('admin.category.index')}}">Категории</a></li>
        <li><a class="dropdown-item" href="{{route('admin.article.index')}}">Материалы</a></li>
      </ul> --}}
