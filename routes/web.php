<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/blog/category/{slug?}', [App\Http\Controllers\BlogController::class, 'category'])->name('category');
Route::get('/blog/article/{slug?}', [App\Http\Controllers\BlogController::class, 'article'])->name('article');

Route::group(['prefix'=>'admin','middleware' => 'role:admin_panel'], function(){
    Route::get('/', [App\Http\Controllers\Admin\DashboardController::class,'dashboard'])->name('admin.index');
    Route::resource('/category',App\Http\Controllers\Admin\CategoryController::class,['as'=>'admin']);
    Route::resource('/article',App\Http\Controllers\Admin\ArticleController::class,['as'=>'admin']);
    Route::group(['prefix'=>'user_managment'],function(){
      Route::resource('/user',App\Http\Controllers\Admin\UserManagment\UserController::class,['as'=>'admin.user_managment']);
    });
});

Route::get('/', function () {
    return view('blog.home');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
